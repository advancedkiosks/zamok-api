# Zamok Browser API

Zamok is Advanced Kiosks product that consists of 2 parts:

1. **Zamok Browser** - secure, isolated browser designed to work on a kiosk.
2. **Zamok Dashboard** - web application, used to manage *Zamok Browser* settings and solutions. A solution can be a web application that runs in browser or a browser addon that typically interacts with some hard-
ware.

This document describes *Zamok Browser API* that is used for webpages to interact with hardware and OS. It should be read by developers that creates new *Zamok* solutions and as a reference for customers that wants to run their own websites in *Zamok Browser*. The API is exposed through javascript as **window.external** object.

## Examples

Examples folder contains very simple examples for each part of Zamok API.

## Credit Card Interpreter API

*Credit Card Interpreter* is a solution that can be used to populate user information by swiping a credit card in a credit card reader. *Standard* usage of this solution is activating the credit card input when browser navigates to a certain page. This API is for *non-standard* usage (like Single Page Applications).

### Preparation

In order to help Browser identify the page you will have to configure the *Credit Card Interpreter* solution in the *Dashboard*. Instead of page URL you have to write your custom page name, which must come in format of
`zamok://pagename`.

### Interface

    window.external.ActivateCC(pagename : String) : void
    window.external.DeactivateCC() : void

**ActivateCC** activates input by credit card swipe. 
  
  *Parameters*:
  
  * **pagename** - the name that you configured in *Preparation* step.

*No return value*

**DeactivateCC** deactivates input by credit card swipe. It must be deactivated before a user tries to input text manually, because our credit card reader intercepts key presses.
  
  *No parameters*

  *No return value*

## Storage API

For privacy, *Zamok Browser* deletes cookies. For browser storage, it exposes another API which uses encryption to protect your data.

### Interface

    window.external.SaveData(appName : String, key : String, data : String) : void
    window.external.LoadData(appName : String, key : String) : String
    window.external.DeleteData(appName : String, key : String) : void

**SaveData** saves data under given application name and key. In order to save objects make use of `JSON.stringify`.

  *Parameters*:
  
  * **appName** - name of your application. Must be consistent when using *Storage API* methods.
  * **key** - data identifier (you will have to use this key to *LoadData*).
  * **data** - stringified data to save.

  *No return value*

**LoadData**

  *Parameters*:
  
  * **appName** - name of your application. Must be consistent when using *Storage API* methods.
  * **key** - data identifier that was previously used with *SaveData*

  *Return value*: data that was previously saved using *SaveData* function. If data is not found, returns *null*

**DeleteData** deletes saved data. If data is not found, does nothing.

  *Parameters*:
  
  * **appName** - name of your application. Must be consistent when using *Storage API* methods.
  * **key** - data identifier (you will have to use this key to *LoadData*).

  *No return value*

## Keyboard API [Example](https://bitbucket.org/advancedkiosks/zamok-api/src/c24ffe762b522bac0242e4f312e8d9aba37bb92a/Examples/Keyboard.html?at=master)

*Zamok Browser* has an embedded on-screen keyboard that can be shown or hidden from our API methods.

### Interface

    window.external.ShowKeyboard()
    window.external.HideKeyboard()

**ShowKeyboard** shows on-screen keyboard centered on the bottom of the screen.

  *No parameters*

  *No return value*

**HideKeyboard** hides on-screen keyboard. Does nothing if it's not visible.

  *No parameters*

  *No return value*

## Printing API

*Zamok Browser* can print visitor badges or a HTML page.

### Interface

    window.external.RenderAccessBadge(code : String, name : String, numAttributes : Integer, attributes : function(int):String) : String
    window.external.PrintAccessBadge(base64 : String, callback : function
    window.external.PrintPage(url : String, callback : function)

**RenderAccessBadge**

  *Parameters*:
  
  * **code** - the barcode.
  * **name** - full visitor name.
  * **numAttributes** - number of additional attributes.
  * **attributes** - a function that returns attribute by it's zero-based index. The function return value should be an object that has 2 fields: `‘key‘` and `‘value‘`, etc. `{ key: ’Year’, value: ’2014’ }`.

  *Return value*: base64 encoded image string.

**PrintAccessBadge** prints the badge, typically rendered with *RenderAccessBadge*.

  *Parameters*:
  
  * **base64** - base64 encoded image string.
  * **callback** - a callback function that gets called when print is complete.

  *No return value*

**PrintPage** prints a rendered html page from given url. **WORK IN PROGRESS**

  *Parameters*:
  
  * **url** - page url.
  * **callback** - a callback function that gets called when print is complete.

  *No return value*

## Scanning API

*Zamok Browser* can scan images and return scan result into your webpage. **WORK IN PROGRESS**

### Interface

    window.external.IsScannerAvailable() : bool
    window.external.Scan(callback : function(string))

**IsScannerAvailable** checks whether kiosk has a scanner attached and online.

  *No parameters*

  *Return value*: returns *true*, if scanner is available. *false* otherwise.

**Scan**

  *Parameters*:
  
  * **callback** is invoked with a base64 encoded image string result from scan.

  *No return value*

## Mailing API

*Zamok Browser* can help you send emails. **WORK IN PROGRESS**

### Interface

    window.external.SendEmail(to : String, subject : String, body : String, numberOfAttachments : Integer, getAttachment : function(int):String) : void

**SendEmail** sends email. It will not fail even if there is no internet connection.
  
  *Parameters*:
  
  * **to** - recipient email address
  * **subject** - email subject
  * **body** - email body
  * **numberOfAttachments** - number of attachments
  * **getAttachment** - a function that returns a stringified attachment given zero-based index

  *No return value*